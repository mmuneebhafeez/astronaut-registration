﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Astronaut.Models;
using iText.Html2pdf;
using OnlineRegistration.Logger;

namespace Astronaut.Controllers
{
    public class MediaController : Controller
    {
        AstronautEntities db = new AstronautEntities();
        // GET: Media
        public ActionResult Index()
        {
            ViewBag.lstCountry = db.Countries.ToList();
            return View();
        }

        [HttpPost]
        public ActionResult uplaodFrontEmiratesID()
        {
            try
            {

                string name = Guid.NewGuid().ToString();
                if (Request.Files.Count > 0)
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;
                        string path = "";

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                            string extesion = fname.Split('.').Last();
                            fname = name + "." + extesion;
                            path = "SampleEmail/EmiratesIDFront/" + fname;
                        }
                        else
                        {
                            string extesion = file.FileName.Split('.').Last();
                            fname = name + "." + extesion;
                            path = "SampleEmail/Media/EmiratesIDFront/" + fname;

                        }

                        TempData["frontID"] = path;
                        // Get the complete folder path and store the file inside it. 
                        fname = Path.Combine(Server.MapPath("~/SampleEmail/Media/EmiratesIDFront/"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                }
                return Json(new { success = true, message = "uploaded!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("MediaController.cs", "uplaodFrontEmiratesID", ex, "HttpPost");
                return Json(new { success = false, error = ex, message = "Registration not successful!" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult uplaodBackEmiratesID()
        {
            try
            {
                string name = Guid.NewGuid().ToString();
                if (Request.Files.Count > 0)
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;
                        string path = "";

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                            string extesion = fname.Split('.').Last();
                            fname = name + "." + extesion;
                            path = "SampleEmail/EmiratesIDFront/" + fname;
                        }
                        else
                        {
                            string extesion = file.FileName.Split('.').Last();
                            fname = name + "." + extesion;
                            path = "SampleEmail/Media/EmiratesIDBack/" + fname;
                        }
                        TempData["BackID"] = path;

                        // Get the complete folder path and store the file inside it. 
                        fname = Path.Combine(Server.MapPath("~/SampleEmail/Media/EmiratesIDBack/"), fname);
                        file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                }
                return Json(new { success = true, message = "uploaded!" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("MediaController.cs", "uplaodBackEmiratesID", ex, "HttpPost");
                return Json(new { success = false, error = ex, message = "Registration not successful!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveRegistration(MediaRegistration lst)
        {
            try
            {
                AppLogger.AppLoggerEntry("Media: start");

                string front = "";
                string back = "";
                if (TempData["frontID"] != null)
                {
                    front = TempData["frontID"].ToString();
                }
                if (TempData["BackID"] != null)
                {
                    back = TempData["BackID"].ToString();
                }

                var r = new Regex(@"(^[a-z])|\.\s+(.)", RegexOptions.ExplicitCapture);

                // MatchEvaluator delegate defines replacement of setence starts to uppercase
                lst.FirstName = r.Replace(lst.FirstName.ToLower(), s => s.Value.ToUpper());
                lst.LastName = r.Replace(lst.LastName.ToLower(), s => s.Value.ToUpper());


                lst.EmiratesIDFront = front;
                lst.EmiratesIDBack = back;

                db.MediaRegistrations.Add(lst);
                db.SaveChanges();

                AppLogger.AppLoggerEntry("Media: saved!");
                Session["ID"] = lst.ID;

                SendMail(lst);

                return Json(new { success = true, message = "Register successful!" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ErrorLogger.Log("MediaController.cs", "SaveRegistration", ex, "HttpPost");
                return Json(new { success = false, error = ex, message = "Registration not successful!" }, JsonRequestBehavior.AllowGet);
            }
        }
        void SendMail(MediaRegistration register)
        {
            try
            {

                string body = "", path = "";
                MailMessage Msg = new MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress("astronauts@mbrsc.ae", "Astronaut Event");

                // Recipient e-mail address.
                Msg.To.Add(register.Email);

                //Msg.Bcc.Add("muneeb@evento.ae");
                Msg.Subject = "Zayed’s Ambition Event – Media Registration ";

                path = AppDomain.CurrentDomain.BaseDirectory + "\\SampleEmail\\Media.html";

                body = System.IO.File.ReadAllText(path);

                body = body.Replace("{name}", register.FirstName + " " + register.LastName);
                body = body.Replace("{ID}", register.ID.ToString());
                Msg.Body = body;
                Msg.IsBodyHtml = true;

                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient("smtp.sendgrid.net", 587);
                smtp.Credentials = new System.Net.NetworkCredential("mohsinxmushtaq", "mohsin@123");
                smtp.EnableSsl = true;
                smtp.Send(Msg);

                //Create PDF of Email
                string invoice = AppDomain.CurrentDomain.BaseDirectory + "\\SampleEmail\\Media\\Invoices\\Invoice" + register.ID + ".pdf";
                if (!Directory.Exists(invoice))
                {
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\SampleEmail\\Media\\Invoices");

                }
                FileStream fileStream = new FileStream(invoice, FileMode.Create, FileAccess.ReadWrite);
                HtmlConverter.ConvertToPdf(body, fileStream);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("MediaController.cs", "SendMail", ex, "HttpPost");

            }
        }

        public ActionResult Thankyou()
        {
            if (Session["ID"] != null)
            {
                return View();

            }
            return RedirectToAction("Index","Media");
        }
    }
}