﻿using Astronaut.Models;
using iText.Html2pdf;
using OnlineRegistration.Logger;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Astronaut.Controllers
{
    public class RegistrationController : Controller
    {

        AstronautEntities db = new AstronautEntities();
        // GET: Registration

        public ActionResult IndividualRegistration()
        {
            ViewBag.lstCountry = db.Countries.ToList();
            return View();

        }
        [HttpPost]
        public ActionResult SaveRegistration( Registration lst)
        {
            try
            {
                AppLogger.AppLoggerEntry("start");

                string front = "";
                string back = "";
                string passport = "";
                if (TempData["frontID"] != null)
                {
                    front = TempData["frontID"].ToString();
                }
                if (TempData["BackID"] != null)
                {
                    back = TempData["BackID"].ToString();
                }
                if (TempData["Passport"] != null)
                {
                    passport = TempData["Passport"].ToString();
                }

                var sessionslst = lst.EventSummary.Split(',');
                string sessionsString = "";
                string sessionTable = "";
                AppLogger.AppLoggerEntry(lst.EventSummary);

                foreach (var item in sessionslst)
                {
                    AppLogger.AppLoggerEntry(item);
                    string temp = Convert.ToString(item);
                    var obj = db.Sessions.Where(w => w.ID.ToString() == temp).FirstOrDefault();
                    if (obj != null)
                    {
                        sessionsString += obj.SessionName + ",";
                        sessionTable += "<tr><td>" + obj.SessionName + "</td><td>" + Convert.ToDateTime(obj.Date).ToString("dd/MM/yyyy") + "</td><td>" + obj.StartTime + " - " + obj.EndTime + "</td></tr>";
                    }
                    AppLogger.AppLoggerEntry(sessionTable);
                    AppLogger.AppLoggerEntry(sessionTable);
                }

                var r = new Regex(@"(^[a-z])|\.\s+(.)", RegexOptions.ExplicitCapture);

                // MatchEvaluator delegate defines replacement of setence starts to uppercase
                lst.FirstName = r.Replace(lst.FirstName.ToLower(), s => s.Value.ToUpper());
                lst.LastName = r.Replace(lst.LastName.ToLower(), s => s.Value.ToUpper());
                lst.EventSummary = sessionsString;


                lst.EmiratesIDFront =  front;
                lst.EmiratesIDBack = back;
                lst.PassportPath = passport;

                db.Registrations.Add(lst);
                db.SaveChanges();

                AppLogger.AppLoggerEntry("saved!");
                Session["ID"] = lst.ID;

                SendMail(lst, sessionTable);

                return Json(new { success = true, message = "Register successful!" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ErrorLogger.Log("RegistrationController.cs", "SaveRegistration", ex, "HttpPost");
                return Json(new { success = false, error = ex, message = "Registration not successful!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult checkCapacity(int _SessionID)
        {
            try
            {
                if (db.Registrations.Count() == 0)
                {
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                int count = 0;
                switch (_SessionID)
                {
                    case 1:
                        count = (int)db.Registrations.Where(x => x.Status == 2).Select(s => s.LaunchEvent).Sum().GetValueOrDefault();
                        break;
                    case 2:
                        count = (int)db.Registrations.Where(x => x.Status == 2).Select(s => s.NASALiveCall).Sum().GetValueOrDefault();
                        break;
                    case 3:
                        count = (int)db.Registrations.Where(x => x.Status == 2).Select(s => s.HamRadio1).Sum().GetValueOrDefault();
                        break;
                    case 4:
                        count = (int)db.Registrations.Where(x => x.Status == 2).Select(s => s.HamRadio2).Sum().GetValueOrDefault();
                        break;
                    case 5:
                        count = (int)db.Registrations.Where(x => x.Status == 2).Select(s => s.RussiaLiveCall1).Sum().GetValueOrDefault();
                        break;
                    case 6:
                        count = (int)db.Registrations.Where(x => x.Status == 2).Select(s => s.RussiaLiveCall2).Sum().GetValueOrDefault();
                        break;
                    case 7:
                        count = (int)db.Registrations.Where(x => x.Status == 2).Select(s => s.JAXAEducationalEvent).Sum().GetValueOrDefault();
                        break;
                    case 8:
                        count = (int)db.Registrations.Where(x => x.Status == 2).Select(s => s.RussiaLiveCall3).Sum().GetValueOrDefault();
                        break;
                    case 9:
                        count = (int)db.Registrations.Where(x => x.Status == 2).Select(s => s.LiveFeedLanding).Sum().GetValueOrDefault();
                        break;
                    default:
                        break;
                }

                int capacity = (int)db.Sessions.Where(p => p.ID == _SessionID && p.Status == true).Select(z => z.Capacity).FirstOrDefault();
                if (count >= capacity)
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, SeatsLeft = capacity - count }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                ErrorLogger.Log("RegistrationController.cs", "checkCapacity", ex, "HttpGet");
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GroupRegistration()
        {
            ViewBag.lstCountry = db.Countries.ToList();
            return View();

        }

        public ActionResult SaveGroupRegistration(Registration lst, List<Student> _stu)
        {
            string sessionsString = "";
            string sessionTable = "";
            var sessionslst = lst.EventSummary.Split(',');
            foreach (var item in sessionslst)
            {
                var obj = db.Sessions.Where(w => w.ID.ToString() == item).FirstOrDefault();
                if (obj != null)
                {

                    sessionsString += obj.SessionName + ",";
                    sessionTable += "<tr><td>" + obj.SessionName + "</td><td>" + Convert.ToDateTime(obj.Date).ToString("dd/MM/yyyy") + "</td><td>" + obj.StartTime + " - " + obj.EndTime + "</td></tr>";

                }
            }
            string front = "";
            string back = "";
            string passport = "";
            if (TempData["frontID"] != null)
            {
                front = TempData["frontID"].ToString();
            }
            if (TempData["BackID"] != null)
            {
                back = TempData["BackID"].ToString();
            }
            if (TempData["Passport"] != null)
            {
                passport = TempData["Passport"].ToString();
            }

            try
            {
                var r = new Regex(@"(^[a-z])|\.\s+(.)", RegexOptions.ExplicitCapture);

                // MatchEvaluator delegate defines replacement of setence starts to uppercase
                lst.FirstName = r.Replace(lst.FirstName.ToLower(), s => s.Value.ToUpper());
                lst.LastName = r.Replace(lst.LastName.ToLower(), s => s.Value.ToUpper());
                lst.EventSummary = sessionsString;

                lst.EmiratesIDFront = front;
                lst.EmiratesIDBack = back;
                lst.PassportPath = passport;


                db.Registrations.Add(lst);
                db.SaveChanges();
                Session["ID"] = lst.ID;
                int tid = lst.ID;

                db.SaveChanges();

                foreach (var item in _stu)
                {
                    item.FK_Registration = tid;
                    item.Status = 2;
                    db.Students.Add(item);
                    db.SaveChanges();
                }
                SendMail(lst, sessionTable);
                return Json(new { success = true, message = "Register successful!" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                ErrorLogger.Log("RegistrationController.cs", "SaveGroupRegistration", ex, "HttpPost");
                return Json(new { success = false, message = "Registration not successful!" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Welcome()
        {
            return View();
        }

        void SendMail(Registration register, string dates)
        {
            try
            {

                string body = "", path = "", tableHTML = "", row = "";
                MailMessage Msg = new MailMessage();
                // Sender e-mail address.
                Msg.From = new MailAddress("astronauts@mbrsc.ae", "Astronaut Event");

                // Recipient e-mail address.
                Msg.To.Add(register.Email);

                //Msg.Bcc.Add("muneeb@evento.ae");
                Msg.Subject = "Zayed’s Ambition Event – Registration Confirmation ";

                if (register.RegistrationType == "Individual")
                    path = AppDomain.CurrentDomain.BaseDirectory + "\\SampleEmail\\Individual.html";
                else
                {
                    path = AppDomain.CurrentDomain.BaseDirectory + "\\SampleEmail\\Group.html";
                    Msg.Subject = "Zayed’s Ambition Event - Group Confirmation ";
                }

                body = System.IO.File.ReadAllText(path);

                if (register.RegistrationType == "Group")
                {
                    var lstChild = db.Students.Where(x => x.FK_Registration == register.ID).ToList();
                    foreach (var item in lstChild)
                    {
                        row = @"<tr>
                        <td align = 'left' > <p style = 'font-size: 14px; line-height: 24px; color: #000000; font-family: 'Roboto', sans-serif; font-weight: normal;  padding:3px;' ><b > {{reg}} </b ></p>        </td >
                        <td align = 'left' > <p style = 'font-size: 14px; line-height: 24px; color: #000000; font-family: 'Roboto', sans-serif; font-weight: normal;  padding:3px;' ><b > {{fullname}} </b ></p>        </td >
                        </tr >";

                        row = row.Replace("{{reg}}", item.ID.ToString());
                        row = row.Replace("{{fullname}}", item.FirstName + " " + item.LastName);
                        row = row.Replace("{{CID}}", item.ID.ToString());
                        tableHTML += row;
                    }
                }

                body = body.Replace(" {{EventTableBody}}", dates);
                body = body.Replace(" {{tbody will come here}}", tableHTML);
                body = body.Replace("{name}", register.FirstName + " " + register.LastName);
                body = body.Replace("{ID}", register.ID.ToString());
                Msg.Body = body;
                Msg.IsBodyHtml = true;

                // your remote SMTP server IP.
                SmtpClient smtp = new SmtpClient("smtp.sendgrid.net", 587);
                smtp.Credentials = new System.Net.NetworkCredential("mohsinxmushtaq", "mohsin@123");
                smtp.EnableSsl = true;
                smtp.Send(Msg);

                //Create PDF of Email
                string invoice = AppDomain.CurrentDomain.BaseDirectory + "\\SampleEmail\\Invoices\\Invoice" + register.ID + ".pdf";
                if (!Directory.Exists(invoice))
                {
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\SampleEmail\\Invoices");

                }
                FileStream fileStream = new FileStream(invoice, FileMode.Create, FileAccess.ReadWrite);
                HtmlConverter.ConvertToPdf(body, fileStream);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("RegistrationController.cs", "SendMail", ex, "HttpPost");

            }
        }

        [HttpPost]
        public ActionResult uplaodFrontEmiratesID()
        {
            string name = Guid.NewGuid().ToString();
            if (Request.Files.Count > 0)
            {
                //  Get all files from Request object  
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];
                    string fname;
                    string path = "";

                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                        string extesion = fname.Split('.').Last();
                        fname = name + "." + extesion;
                        path = "SampleEmail/EmiratesIDFront/" + fname;
                    }
                    else
                    {
                        string extesion = file.FileName.Split('.').Last();
                        fname = name + "." + extesion;
                        path = "SampleEmail/EmiratesIDFront/" + fname;

                    }

                    TempData["frontID"] = path;
                    // Get the complete folder path and store the file inside it. 
                    fname = Path.Combine(Server.MapPath("~/SampleEmail/EmiratesIDFront/"), fname);
                    file.SaveAs(fname);

                }
                // Returns message that successfully uploaded  
            }
            return Json(new { success = true, message = "uploaded!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult uplaodBackEmiratesID()
        {
            string name = Guid.NewGuid().ToString();
            if (Request.Files.Count > 0)
            {
                //  Get all files from Request object  
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];
                    string fname;
                    string path = "";

                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                        string extesion = fname.Split('.').Last();
                        fname = name + "." + extesion;
                        path = "SampleEmail/EmiratesIDFront/" + fname;
                    }
                    else
                    {
                        string extesion = file.FileName.Split('.').Last();
                        fname = name + "." + extesion;
                        path = "SampleEmail/EmiratesIDBack/" + fname;

                    }

                    TempData["BackID"] = path;
                    // Get the complete folder path and store the file inside it. 
                    fname = Path.Combine(Server.MapPath("~/SampleEmail/EmiratesIDBack/"), fname);
                    file.SaveAs(fname);


                }
                // Returns message that successfully uploaded  
            }
            return Json(new { success = true, message = "uploaded!" }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult uplaodPassport()
        {
            string name = Guid.NewGuid().ToString();
            if (Request.Files.Count > 0)
            {
                //  Get all files from Request object  
                HttpFileCollectionBase files = Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];
                    string fname="", extesion="", path="";

                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                        extesion = fname.Split('.').Last();
                        fname = name + "." + extesion;
                        path = "SampleEmail/EmiratesIDFront/" + fname;
                    }
                    else
                    {
                        extesion = file.FileName.Split('.').Last();
                        fname = name + "." + extesion;
                        path = "SampleEmail/EmiratesIDBack/" + fname;

                    }

                    TempData["Passport"] = path;

                    // Get the complete folder path and store the file inside it. 
                    fname = Path.Combine(Server.MapPath("~/SampleEmail/Passport/"), fname);
                    file.SaveAs(fname);


                }
                // Returns message that successfully uploaded  
            }
            return Json(new { success = true, message = "uploaded!" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult comingsoon()
        {
            return View();
        }


        public ActionResult Thankyou()
        {
            if (Session["ID"] != null)
            {
                return View();
            }
            return RedirectToAction("Welcome", "Registration");
        }
    }
}